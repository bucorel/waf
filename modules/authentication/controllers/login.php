<?php
	if( isset( $_POST['_act'] ) && $_POST['_act'] == 1 ){
		if( !isset( $_POST['loginName']) || $_POST['loginName'] == "" ){
			$this->sendError( 'Enter Login Name', 'loginName' );
		}else if( !isset( $_POST['password']) || $_POST['password'] == "" ){
			$this->sendError( 'Enter Password', 'password' );
		}else{
			$this->sendError("Authentication Failure");
		}
		
	}
	
	/* define view for login process */
	$view = WAF_SYSTEM_PATH."modules/authentication/views/login.view.php";
	
	if( !file_exists( $view ) ){
		$this->sendError("View is missing");
	}
	
	/* load the view */
	include $view;
	
?>
