<?php
	$html = '<div class="formholder">';
	$html .='<form method="post" action="" onsubmit="return false;" autocomplete="off">';
	
	$html .='<div class="row">';
	$html .='<div class="label">Login Name / ID</div>';
	$html .='<div class="element">';
	$html .='<input type="text" name="loginName" id="loginName" />';
	$html .='</div>';
	$html .='</div>';

	$html .='<div class="row">';
	$html .='<div class="label">Password</div>';
	$html .='<div class="element">';
	$html .='<input type="password" name="password" id="password" />';
	$html .='</div>';
	$html .='</div>';

	$html .='<div class="row">';
	$html .='<div class="btngroup">';
	$html .='<input type="submit" value="Login" onclick="pf(\'\',this.form)"/>';
	$html .='</div>';
	$html .='</div>';
	
	$html .='<input type="hidden" name="_mod" value="authentication" />';
	$html .='<input type="hidden" name="_con" value="login" />';
	$html .='<input type="hidden" name="_act" value="1" />';
	$html .='</form>';
	$html .='</div>';
	
	$this->fill("heading","Authentication");
	$this->fill("content_space",$html);
	$this->execJs("setFocus('loginName')");
	$this->end();
	
?>
