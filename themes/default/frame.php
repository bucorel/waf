<?php
	/* In WAF every theme consists atleast 3 files, frame.php, frame.css and 
	frame.js. You can create as many css/js files as required. Loading css and
	js this way has multiple advantages, first browser does not create multiple
	requests to load these files and second we can do any pre-loading-operation
	like javascript compresion etc */
	$cssFile = dirname(__FILE__)."/frame.css";
	$jsFile = dirname(__FILE__)."/frame.js";
?>
<!DOCTYPE html>
<html>
	<head>
		<title><?php echo APPLICATION_FULL_NAME; ?></title>
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<?php
		if( file_exists( $cssFile ) ){
			echo "<style>";
			include $cssFile;
			echo "</style>";
		}
		
		if( file_exists( $jsFile ) ){
			echo "<script>";
			include $jsFile;
			echo "</script>";
		}
		?>
	</head>
	<body>
		<div class="page">
			<div class="banner" id="banner"><div class="inner"><div class="brand" id="brand"><?php echo APPLICATION_FULL_NAME; ?></div><div class="userctrl" id="userctrl"></div></div></div>
			<div class="menu" id="menu">
				<div class="inner" id="menuholder">
					<div class="clear"></div>
				</div>
			</div>
			<div class="heading"><div class="inner" id="heading">Page Heading</div></div>
			<div class="content_space"><div class="inner" id="content_space"></div></div>
			<div class="footer" id="footer"><div class="inner" >&copy; <a href="<?php echo COPYRIGHT_HOLDER_URL; ?>"><?php echo COPYRIGHT_HOLDER; ?></a>. All rights reserved</div></div>
			<div class="status" id="status"></div>
			<div class="progress_indicator" id="progress_indicator">LOADING</div>
		</div>
		<script>
		init();
		</script>
	</body>
</html>
