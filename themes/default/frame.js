function init(){
	var d = document.getElementById("content_space");
	var h = window.innerHeight;
	var h1 = document.getElementById("banner").offsetHeight;
	var h2 = document.getElementById("footer").offsetHeight;
	var h3 = document.getElementById("menu").offsetHeight;
	var h4 = document.getElementById("heading").offsetHeight;
	var h5 = h - (h1 + h2 + h3 + h4 + 40);
	
	if( d.offsetHeight < h5 ){
		d.style.height = h5 + "px";
	}
	
	pq("","_mod=home&_con=start");
}

function pindi(d){
	if( document.getElementById('progress_indicator') ){
		var p=document.getElementById('progress_indicator');
		if(d==1){
			p.style.display='initial';
		}else{
			p.style.display='none';
		};
	};
};

function post(adr,data){
	pindi(1);
	var xhr = new XMLHttpRequest();
	xhr.open('POST',adr,true);
	xhr.onreadystatechange=function(){
		if(xhr.readyState==4){
			if(xhr.status==200){
				processData(xhr.responseText);
			}else{
				showStatus(0,xhr.status);
			}
			pindi(0);
		}
	};
	xhr.send(data);
};

function processData( t ){
	try{
		var j = JSON.parse(t);
	}catch(e){
		showStatus(0,e.message);
		return;
	};
	
	if( j['_mes'] != "" && j['_fie'] != "" ){
		showFieldError( j['_fie'], j['_mes']);
	}else if( j['_mes'] != "" && j['_fie']=="" ){
		showStatus(j['_sta'],j['_mes']);
	};
	
	for(var i in j['_dat']){
		var r=j['_dat'][i];
		if(r['_typ']=='fill'){
			fill(r['_id'],r['_htm']);
		}else if(r['_typ']=='js'){
			eval(r['_htm']);
		};
	};
	
	if(j['_fie']!=""){
		setFocus(j['_fie']);
	};
};

function fill(id,txt){
	if(document.getElementById(id)){
		if(id=='popup'){
			document.getElementById(id).style.display='initial';
		};
		document.getElementById(id).innerHTML=txt;
	};
};

function setFocus(id){
	if(document.getElementById(id)){
		document.getElementById(id).focus();
	};
};

var removing=false;
var lastZ = 0;
function showStatus(t,m){
	if(document.getElementById('status')){
		var d=document.createElement('div');
		d.innerHTML=m;
		if(t==0){
			d.className='error';
		}else{
			d.className='ok';
		};
		document.getElementById('status').appendChild(d);
		if(removing==false){
			removing=true;
			setTimeout('removeStatus()',3000);
		};
		lastZ=lastZ+1;
		document.getElementById('status').style.zIndex=lastZ;
	};
};

function removeStatus(){
	if(document.getElementById('status')){
		var s=document.getElementById('status');
		if(s.childNodes.length==0){
			removing=false;
			return;
		};
		
		s.removeChild(s.childNodes[0]);
		setTimeout('removeStatus()',3000);
	};
};

function pq(adr,qs){
	var fd = new FormData();
	var p=qs.split('&');
	for(var i in p){
		var kv=p[i].split('=');
		if(kv.length==2){
			fd.append(kv[0],kv[1]);
		};
	};
	
	post(adr,fd);
};

function pf(adr,f){
	var fd = new FormData(f);
	post(adr,fd);
};

function showFieldError( id, msg ){
	remFieldError();
	var d=document.createElement('div');
	d.setAttribute('id','fieldError');
	d.innerHTML = msg;
	document.body.appendChild(d);
	posFieldError(id,d);
};

function remFieldError(){
	if( document.getElementById('fieldError')){
		document.body.removeChild( document.getElementById('fieldError') );
	};
};

function posFieldError( id,t ){
	if( document.getElementById(id)){
		var d=document.getElementById(id);
		var top=d.offsetTop + d.offsetHeight;
		var left=d.offsetLeft;
		t.style.left=left+'px';
		t.style.top=top+'px';
		setTimeout( 'remFieldError()',2000);
	};
};

