<?php
	/* APPLICATION PARAMETERS *************************************************/
	define( "APPLICATION_FULL_NAME", "Web Application Framework" );
	define( "COPYRIGHT_HOLDER", "Business Computing Research Laboratory" );
	define( "COPYRIGHT_HOLDER_URL", "http://www.bucorel.com" );
	
	
		
	/* PATH PARAMETERS ********************************************************/
	
	/* CAUTION - Do not put system and upload folder in Document Root or publicly
	accessible folder */
	
 	/* SYSTEM_PATH - where entire system is stored. Default value is ./system/
 	Do not forget to put a "/" after the folder name */
	define( "WAF_SYSTEM_PATH", "system/" );
	
	/* UPLOAD_PATH - uploaded files will be stored here. Default value is 
	./upload/ Do not forget to put a "/" after the folder name */
	define( "WAF_UPLOAD_PATH", "upload/" );
	
	/* MEDIA_PATH - where all the common media files like images, videos etc
	are stored */
	define( "WAF_MEDIA_PATH", "media/" );
	
	
	
	/* DATABASE ***************************************************************/
	
	/* DATABASE_HOST - IP or DNS of the database server */
	define( "WAF_DATABASE_HOST", "" );
	
	/* DATABASE_PORT - Integer like 5432 for PostgreSQL default port */
	define( "WAF_DATABASE_PORT", "" );
	
	/* DATABASE_NAME - Name of the database you would like to connect */
	define( "WAF_DATABASE_NAME", "" );
	
	/* DATABASE_USER - Username/ID */
	define( "WAF_DATABASE_USER", "" );
	
	/* DATABASE_PASSWORD - Password for the above given username */
	define( "WAF_DATABASE_PASSWORD", "" );
	
	
	
	/* LOCALISATION ***********************************************************/
	
	/* Default Language - if no language is selected, system will show interfaces
	in this language */
	define( "WAF_DEFAULT_LANGUAGE", "en" );
	
	/* Default Timezone - Default value is Asia/Kolkata. To understand timezone 
	IDs read this wikipedia page - 
	https://en.wikipedia.org/wiki/List_of_tz_database_time_zones */
	define( "WAF_DEFAULT_TIMEZONE", "Asia/Kolkata" );
	
	/* CURRENCY_PREFIX - represents ASCII/UTF-8 symbol or a htmlentity like &euro; */
	define( "WAF_DEFAULT_CURRENCY_PREFIX", "&euro;" );
	
	/* DEFAULT_NUMBER_OF_DECIMAL_PLACES - how many decimal places in currency */
	define( "WAF_DEFAULT_NUMBER_OF_DECIMAL_PLACES", 2 );
	
	/* DEFAULT_DATE_FORMAT - show date in this format. To know more about PHP date
	formats, visit this page http://php.net/manual/en/function.date.php */
	define( "WAF_DEFAULT_DATE_FORMAT", "d-M-Y" );
	
	
	
	/* THEME ******************************************************************/
	
	/* themes are stored under SYSTEM_PATH/themes/ Theme name represent a folder
	under this path for example SYSTEM_PATH/themes/default */
	define( "WAF_DEFAULT_THEME", "default" );
	
	
	
	/* MISCELLANIOUS SETTINGS *************************************************/
	
	
	
	/* SESSION ****************************************************************/
	
	/* SESSION_NAME - PHP creates a session cookie called PHPSESSID. If you wish 
	to change cookie name, set the name here */
	define( "WAF_SESSION_NAME", "WAF" );
	
	/* SESSION_STORAGE - where to store session data? on file or on memcache */
	define( "WAF_SESSION_STORE", "file" );
	
	/* SESSION_PATH - in case session is stored on memcache, this is going to be
	the address (url with parameters) of the memcache server */
	define( "WAF_SESSION_PATH", "" );
?>
