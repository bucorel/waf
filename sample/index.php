<?php
	/* Lets load configuration first */
	require_once( "config.php" );
	
	/* Now loading WAF (Web Application FrameWork) class, this will handle all 
	the requests */
	require_once( WAF_SYSTEM_PATH."library/core/AjaxResponse.class.php" );
	require_once( WAF_SYSTEM_PATH."library/core/Waf.class.php" );
	
	/* Lets WAF handle everything from this point onwards */
	$waf = new Waf();
	$waf->handleRequest();
?>
