<?php

/* 
WAF - Web Application FrameWork class 
this class intercepts requests, validates it and loads the required controller.
The controller then processes the request and loads views.
*/

class Waf extends AjaxResponse{
	
	/* we can store session data on files or memcache */
	protected $sessionStorage = 'file';
	
	function setSessionStorage( $type ){
		$this->sessionStorage = $type;
	}
	
	function handleRequest(){
		/* we are going to handle only HTTP Get and Post methods, everything else
		will be ignored. If other method is required, you can add a custom 
		handler for it */
		switch( $_SERVER['REQUEST_METHOD'] ){
			case 'GET':
				$this->handleGetRequest();
				break;
			case 'POST':
				$this->handlePostRequest();
				break;
			default:
				break;
		}
	}
	
	function handleGetRequest(){
		/* lets setup the session first */
		$this->sessionSetup();
		
		/* AFW's ajax client only creates POST requests. If it is a GET request
		it means either the ajax client is not loaded yet ( when client typed 
		the address in browser's address bar and hit enter ) or client's browser
		is forced to refresh. In this case we will load the ajax client which in
		a part of theme. So lets load theme (template) */
		$this->loadTheme();
	}
	
	function handlePostRequest(){
		/* system expect two parameters to load a controller. They are _mod 
		(module) and _con (controller). System looks into the "SYSTEM_PATH/modules"
		for this module and the controller file */
		
		/* checking whether _mod is specified or not */
		if( !isset($_POST['_mod']) || $_POST['_mod']=="" ){
			$this->sendError( 'Invalid Module' );
		}
		
		/* checking whether _con is specified or not */
		if( !isset($_POST['_con']) || $_POST['_con']=="" ){
			$this->sendError('Invalid Controller');
		}
		
		/* creating file name with full path */
		$controller = WAF_SYSTEM_PATH.'modules/'.$_POST['_mod'].'/controllers/'.$_POST['_con'].'.php';
		
		
		if( !file_exists( $controller ) ){
			$this->sendError( 'Module or Controller does not exist' );
		}
		
		/* time to initialise session */
		$this->sessionSetup();
		
		/* lets handover the controls to the controller */
		include $controller;
	}
	
	function loadTheme(){
		/* SYSTEM_PATH and DEFAULT_THEME are defined in config.php 
		frame.php provides all the common html components like headers, footer
		etc. It also loads CSS and the AJAX client (javascript) */
		$themeFile = WAF_SYSTEM_PATH.'/themes/'.WAF_DEFAULT_THEME.'/frame.php';
		if( file_exists( $themeFile ) ){
			include $themeFile;
		}else{
			header('HTTP/1.1 404 Not Found');
			echo 'Theme <b>'.WAF_DEFAULT_THEME.'</b> does not exist';
		}
	}
	
	function sessionSetup(){
		if( $this->sessionStorage == 'memcache' ){
			ini_set( 'session.save_handler', 'memcache' );
			ini_set( 'session.save_path', WAF_SESSION_PATH );
		}
		
		session_name( WAF_SESSION_NAME );
		session_start();
	}
}
?>
