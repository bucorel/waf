<?php

/*
AjaxResponse class creates JSON based response. A javascript based client will be
required to parse and process JSON responses. 

AjaxResponse class does not create entire output before sending it to the client.
it creates and send output in fragments to reduce server side memory usage.  
*/

class AjaxResponse{
	
	const STATUS_OK = 1;
	const STATUS_ERROR = 0;
	
	/* this indicates whether header has been sent or not */
	protected $started = false;
	
	/* this indicates whether footer has been sent or not */
	protected $ended = false;
	
	/* this indicates that atleas one fregment has already been sent */
	protected $dataStarted = false;
	
	/* start method sends the header of the reply which consists three elements
	statusCode, message and a field name. Status code tells the client whether
	request processed successfully or not. Message string is used to explain the
	status. If there is an error related to a field or you want to set focus to
	a field, you can put it's name in the Field string 
	
	Example - 0,"Name can not be empty","first_name"
	0 says there is an error, "name can not be empty" is the message and
	"first_name" is the field which requires the value
	*/
	function start( $statusCode, $message="", $fieldName="" ){
		/* lets tell the browser that we are going to send JSON */
		header( "Content-Type:application/json" );
		
		/* we are going to send string literals thats why single quotes are used*/
		echo '{';
		echo '"_sta":'.json_encode( $statusCode ).',';
		echo '"_mes":'.json_encode( $message ).',';
		echo '"_fie":'.json_encode( $fieldName ).',';
		
		/* we send data in fregments. these fragments will be created by the 
		send method. All the fregments will be members of _dat array */
		echo '"_dat":[';
		
		/* this will inform other methods that header has been sent */
		$this->started = true;
	}
	
	function end(){
		/* do this only when header has been sent */
		if( $this->started ){
			/* end of _dat array and entire json doc */
			echo ']}';
			
			/* this tells the destructor that we have already terminated the response */
			$this->ended = true;
			exit;
		}
	}
	
	function __destruct(){
		/* if user has forgotten to call end() method and destruct has arrived */
		if( !$this->ended ){
			$this->end();
		}
	}
	
	/* this method sends a fregment 
	whatToDo holds a pre-defined value which indicates the client what we want it
	to do whith the associated data.
	whereToShow holds id of html component in which we would like to show this data
	data holds anything you want to send
	*/
	function send( $whatToDo, $whereToShow, $data ){
		/* if response header is not yet sent, send it first */
		if( !$this->started ){
			$this->start(1);
		}
		
		if( $this->dataStarted ){
			/* atleast one array element has been sent. To send another one we
			must send a valid array element separator ',' before it */
			echo ',';
		}else{
			/* No fregment sent which means this one is the first */
			$this->dataStarted = true;
		}
		
		echo '{';
		echo '"_typ":'.json_encode( $whatToDo ).',';
		echo '"_id":'.json_encode( $whereToShow ).',';
		echo '"_htm":'.json_encode( $data );
		echo '}';
	}
	
	/* wrappers / shortcuts */
	function sendError( $errorMessage, $fieldName="" ){
		$this->start( self::STATUS_ERROR, $errorMessage, $fieldName );
		$this->end();
	}
	
	function sendOk( $message, $fieldTofocus="" ){
		$this->start( self::STATUS_OK, $message, $fieldToFocus );
		$this->end();
	}
	
	function fill( $divID, $html ){
		$this->send( "fill", $divID, $html );
	}
	
	function execJs( $codeToExecute ){
		$this->send( "js", "", $codeToExecute );
	}
}
?>
