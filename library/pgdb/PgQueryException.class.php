<?php
	class PgQueryException extends Exception{
	
		protected $sql = "";
		
		function __construct( $message = "", $sql = "" ){
			$this->sql = $sql;
			parent::__construct( $message );
		}
	
		function getSql(){
			return $this->sql;
		}

	}
?>
