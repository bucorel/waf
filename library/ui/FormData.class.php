<?php
	/** Please add document description here */
	class FormData{
		protected $fields = array();
		protected $data = array();
		
		/* data types */
		const TYPE_STRING = 1;
		const TYPE_NUMERIC = 2;
		const TYPE_DATE = 3;
		const TYPE_TIME = 4;
		const TYPE_BOOLEAN = 5;
		const TYPE_FILE = 6;
		
		const SUBTYPE_EMAIL = 1;
		const SUBTYPE_IP4 = 2;
		const SUBTYPE_IP6 = 3;
		const SUBTYPE_URL = 4;
		const SUBTYPE_PASSWORD = 5;
		
		const SUBTYPE_INT = 6;
		const SUBTYPE_FLOAT = 7;
		
		function addField( $id, $fullName, $dataType, $required=true ){
			$def = array();
			$def[ 'fullName' ] = $fullName;
			$def[ 'type' ] = $dataType;
			$def[ 'rules' ] = array("required",$required);
			$this->fields[ $id ] = $def;
		}
		
		function setRule( $fieldId, $rule, $value ){
			if( array_key_exists( $fieldId, $this->fields )){
				$this->fields[ $fieldId ][ 'rules' ][ $rule ] = $value;
			}
		}
		
		function getData(){
			foreach( $this->fields as $k=>$v ){
				if( $v['rules']['required']==true && (!isset($_POST[$k]) || $_POST[$k]=="") ){
					throw new ValidationException('Please enter '.$v['fullName'],$k);
				}
				
				if( isset($_POST[$k]) && $_POST[$k] != "" ){
					switch( $v['type'] ){
						case self::TYPE_STRING:
							$this->validateString($k,$v['rules']);
							break;
						case self::TYPE_NUMERIC:
							$this->validateNumeric($k,$v['rules']);
							break;
						case self::TYPE_DATE:
							$this->validateDate($k,$v['rules']);
							break;
						case self::TYPE_TIME:
							$this->validateTime($k,$v['rules']);
							break;
						case self::TYPE_BOOLEAN:
							$this->validateBoolean($k,$v['rules']);
							break;
						case self::TYPE_FILE:
							$this->validateFile($k,$v['rules']);
							break;
						default:
							throw new Exception('UNKNOWN_DATATYPE');
					}
					
					$this->data[$k] = $_POST[$k];
				}
			}
			
			return $this->data;
		}
		
		function validateString($id,$rules){
			$this->data[$id]=$_POST[$id];
		}
		
		function validateNumeric($id,$rules){
			if( !is_numeric($_POST[$id]) ){
				throw new ValidationException('NUMBER_REQUIRED',$id);
			}
			
			$this->data[$id]=$_POST[$id];
		}
		
		function validateDate($id,$rules){
			$this->data[$id]=$_POST[$id];
		}
		
		function validateTime($id,$rules){
			$this->data[$id]=$_POST[$id];
		}
		
		function validateBoolean($id,$rules){
			$this->data[$id]=$_POST[$id];
		}
		
		function validateFile($id,$rules){
			$this->data[$id]=$_POST[$id];
		}
		
	}
?>
