<?php
	/** Please add document description here */

	class HtmlForm{
		protected $groups = array();
		protected $hiddenElements = array();
		
		protected $submitButtonLabel = 'Submit';
		protected $submitButtonCss = 'save_button';
		
		const CSS_FORM_HOLDER = 'form_holder';
		const CSS_FORM_GROUP = 'form_group';
		const CSS_FORM_GROUP_HEADING = 'form_group_heading';
		const CSS_FORM_ROW = 'form_row';
		
		const CSS_FORM_LABEL = 'label';
		const CSS_FORM_ELEMENT = 'element';
		const CSS_FORM_LARGE_LABEL = 'large_label';
		const CSS_FORM_LARGE_ELEMENT = 'large_element';
		const CSS_FORM_FILE = 'file';
		const CSS_FORM_DATE = 'date';
		
		const CSS_FORM_SAVE_BUTTON = 'save_button';
		const CSS_FORM_UPDATE_BUTTON = 'update_button';
		const CSS_FORM_DELETE_BUTTON = 'delete_button';
		
		function __construct( $module, $controller ){
			$this->hiddenElements['_mod'] = $module;
			$this->hiddenElements['_con'] = $controller;
			$this->hiddenElements['_act'] = 1;
		}
		
		function setSubmitButton( $label, $cssClass ){
			$this->submitButtonLabel = $label;
			$this->submitButtonCss = $cssClass;
		}
		
		function addGroup( $groupName, $groupHeading="" ){
			$groupDef = array();
			$groupDef['heading'] = $groupHeading;
			$groupDef['elements'] = array();
			$this->groups[ $groupName ] = $groupDef;
		}
		
		function addRawRow( $groupName, $label="", $element="", $labelCss="", $elementCss="" ){
			$lcss = '';
			$ecss = '';
			if( $labelCss != "" ){
				$lcss = 'class="'.$labelCss.'"';
			}
			
			if( $elementCss != "" ){
				$ecss = 'class="'.$elementCss.'"';
			}
			
			$html = '<div class="'.self::CSS_FORM_ROW.'">';
			
			if( $label != "" ){
				$html .='<div '.$lcss.'>';
				$html .=$label;
				$html .='</div>';
			}
			
			if( $element != "" ){
				$html .='<div '.$ecss.'>';
				$html .=$element;
				$html .='</div>';
			}
			
			$html .='</div>';
			
			if( array_key_exists( $groupName, $this->groups )){
				array_push( $this->groups[ $groupName ]['elements'], $html );
			}
		}
		
		function draw(){
			$html = '<form method="POST" action="" onsubmit="return false;" autocomplete="off">';
			$html .= '<div class="'.self::CSS_FORM_HOLDER.'">';
			
			/* build all groups */
			foreach( $this->groups as $k=>$v ){
				$html .='<div class="'.self::CSS_FORM_GROUP.'">';
				
				if( $v['heading'] != "" ){
					$html .='<div class="'.self::CSS_FORM_GROUP_HEADING.'">'.$v['heading'].'</div>';
				}

				foreach( $v['elements'] as $e ){
					$html .=$e;
				}
				$html .='</div>';
			}
			
			/* build all hidden elements */
			foreach( $this->hiddenElements as $k=>$v ){
				$html .='<input type="hidden" name="'.$k.'" id="'.$k.'" value="'.$v.'" />';
			}
			
			/* build button group */
			$html .='<div class="'.self::CSS_FORM_GROUP.'">';
			$html .='<div class="'.self::CSS_FORM_ROW.'">';
			$html .='<div class="'.self::CSS_FORM_LARGE_ELEMENT.'">';
			$html .='<input type="submit" value="'.$this->submitButtonLabel.'" class="'.$this->submitButtonCss.'" />';

			$html .='</div>';
			$html .='</div>';
			$html .='</div>';
			
			/* end of form_holder */
			$html .='</div>';
			$html .='</form>';
			return $html;
		}
		
		/* wrappers */
		function addTextRow($group,$label,$id,$value=""){
			$e = new HtmlElement('input',$id,$value);
			$e->setAttribute('type','text');
			$this->addRawRow( $group, $label,$e->draw());
		}
		
		function addPasswordRow($group,$label,$id,$value=""){
			$e = new HtmlElement('input',$id,$value);
			$e->setAttribute('type','password');
			$this->addRawRow( $group, $label,$e->draw());
		}
		
		function addSelectRow($group,$label,$id,$value="",array $options){
			$e = new HtmlElement('select',$id,$value);
			foreach($options as $k=>$v){
				$e->setOption($k,$v);
			}
			$this->addRawRow( $group, $label,$e->draw());
		}
		
		function addTextareaRow( $group, $label, $id, $value=""){
			$e = new HtmlElement('textarea',$id,$value);
			$this->addRawRow( $group, $label,$e->draw());
		}
		
		function addFileRow( $group, $label, $id, $value=""){
			$e1 = new HtmlElement('input',$id.'_dummy','');
			$e1->setAttribute("type","text");
			
			$e2 = new HtmlElement('input','b1','Browse');
			$e2->setAttribute("type","button");
			$e2->setAttribute('onclick','document.getElementById(\''.$id.'\').click()');
			
			$e3 = new HtmlElement('input',$id,'');
			$e3->setAttribute('type','file');
			$e3->setAttribute('style','display:none;');
			$e3->setAttribute('onchange','document.getElementById(\''.$id.'_dummy\').value=this.value');
			
			$html ='<div class="'.self::CSS_FORM_FILE.'">';
			$html .=$e1->draw();
			$html .=$e2->draw();
			$html .=$e3->draw();
			
			$html .='</div>';
			$this->addRawRow( $group, $label, $html );
		}
		
		function addDateRow( $group, $label, $id, $value="",$format='dmy'){
			$e1 = new HtmlElement('select',$id.'_dd','');
			for($i=1;$i<32;$i++){
				$e1->setOption($i,$i);
			}
			
			$e2 = new HtmlElement('select',$id.'_mm','');
			for($i=1;$i<13;$i++){
				$e2->setOption($i,$i);
			}
			
			$e3 = new HtmlElement('input',$id.'_yy','');
			$e3->setAttribute('type','text');
			
			$e4 = new HtmlElement('input','','+');
			$e4->setAttribute('type','button');
			$e4->setEvent('onclick','togCal',array($id,'this.value'));
			
			$html ='<div class="'.self::CSS_FORM_DATE.'">';
			if($format=='mdy'){
				$html .=$e2->draw();
				$html .=$e1->draw();
				$html .=$e3->draw();
			}else if( $format=='ymd'){
				$html .=$e3->draw();
				$html .=$e2->draw();
				$html .=$e1->draw();
			}else{
				$html .=$e1->draw();
				$html .=$e2->draw();
				$html .=$e3->draw();
			}
			$html .=$e4->draw();
			$html .='</div>';
			$this->addRawRow( $group, $label, $html );
		}
		
		function addTimeRow( $group, $label, $id, $value=""){
			$e1 = new HtmlElement('select',$id.'_hrs','');
			for($i=0;$i<24;$i++){
				$e1->setOption($i,$i);
			}
			
			$e2 = new HtmlElement('select',$id.'_min','');
			for($i=0;$i<60;$i++){
				$e2->setOption($i,$i);
			}
			
			$e3 = new HtmlElement('select',$id.'_sec','');
			for($i=0;$i<60;$i++){
				$e3->setOption($i,$i);
			}
			
			$html ='<div class="'.self::CSS_FORM_DATE.'">';

			$html .=$e1->draw();
			$html .=$e2->draw();
			$html .=$e3->draw();
			
			$html .='</div>';
			$this->addRawRow( $group, $label, $html );
		}
	}
?>
