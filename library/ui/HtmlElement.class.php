<?php
class HtmlElement{
	/* element type like input, div, select etc */
	protected $type = "";
	
	/* attributes like class, id, name, value etc */
	protected $attributes = array();
	
	/* this element contains values within its start and end tag */
	protected $hasInnerHtml = false;
	
	/* value to show within start and end tag */
	protected $innerHtml = "";
	
	/* in case of a SELECT element, these are going to be the options */
	protected $options = array();
	
	/* element value */
	protected $value = "";
	
	function __construct( $elementType, $name="", $value="" ){
		$elementType = strtolower($elementType);
		$this->type = $elementType;
		if( $name != "" ){
			$this->setAttribute('id',$name);
			$this->setAttribute('name',$name);
		}
		
		switch( $elementType ){
			case 'input':
				$this->setAttribute('value',$value);
			case 'select':
				$this->hasInnerHtml = true;
				$this->value = $value;
				break;
			case 'textarea':
				$this->hasInnerHtml = true;
				$this->innerHtml = $value;
				break;
			default:
		}
		
	}

	function setEvent( $eventName, $functionName, array $params ){
		$functionString = $functionName.'(';
		$functionString .=implode(',',$params);
		$functionString .=');';
		$this->setAttribute( $eventName, $functionString );
	}
	
	function setAttribute( $attributeName, $value ){
		$this->attributes[ $attributeName ] = $value;
	}
	
	function setInnerHtml( $html ){
		$this->innerHtml = $html;
		$this->hasInnerHtml = true;
	}
	
	function setOption( $key, $value ){
		$this->hasInnerHtml = true;
		if($this->value==$key){
			$sel = ' SELECTED';
		}else{
			$sel='';
		}
		$this->innerHtml .='<option value="'.$key.'" '.$sel.'>'.$value.'</option>';
	}
	
	function draw(){
		$html = '<'.$this->type.' ';
		foreach( $this->attributes as $k=>$v ){
			$html .=$k.'="'.$v.'" ';
		}
		
		if( $this->hasInnerHtml ){
			$html .='>';
			if( $this->innerHtml!=""){
				$html .=$this->innerHtml;
			}
			$html .='</'.$this->type.'>';
		}else{
			$html .='/>';
		}
		
		return $html;
	}
	
}
?>
