<?php
	/* DataModel class creates data-structure and automate most of the database
	operations like insert, update, delete etc. It can also validate and filter
	inputs */
	
	/*
	Other classes required by this class
	DataModelException.class.php
	DataValidationException.class.php
	*/
	
	class DataModel{
		
		/* data contains fields and their values in key,value pairs */
		protected $data = array();
		
		/* def - stores field definitions */
		protected $def = array();
		
		/* database connection object */
		protected $connectionObject = null;
		
		/* type of database like mysql, postgresql etc */
		protected $databaseType = 0;
		
		/* tableName - table which is going to be represented by this model */
		protected $tableName = "";
		
		/* idFieldName - which field represents recordId */
		protected $idFieldName = "";
		
		/* Supported database types */
		const DB_PGSQL = 0;
		const DB_MYSQL = 1;
		
		/* supported data types */
		const TYPE_STRING = 0;
		const TYPE_INT = 1;
		const TYPE_FLOAT = 2;
		const TYPE_BOOLEAN = 3;
		const TYPE_DATE = 4;
		const TYPE_TIME = 5;
		
		/* default values of various datatypes */
		protected $defaultValues = array();
		
		function __construct( $dbType, $connectionObject, $tableName, $idFieldName ){
		
			/* lets define default values for each supported datatype */
			$this->defaultValues[ self::TYPE_STRING ] = "";
			$this->defaultValues[ self::TYPE_INT ] = 0;
			$this->defaultValues[ self::TYPE_FLOAT ] = 0.0;
			$this->defaultValues[ self::TYPE_BOOLEAN ] = false;
			$this->defaultValues[ self::TYPE_DATE ] = 0;
			$this->defaultValues[ self::TYPE_TIME ] = 0;
			
			$this->databaseType = $dbType;
			$this->connectionObject = $connectionObject;
			$this->tableName = $tableName;
			$this->idFieldName = $idFieldName;
		}
		
		/* defineField defines a field using field name and data type */
		function defineField( $fieldName, $dataType ){
			/* what to do when key already exists */
			if( array_key_exists( $fieldName, $this->def ) ){
				throw new DataModelException( "DUPLICATE_FIELD" );
			}
			
			/* what to do when data type is unknown */
			if( !array_key_exists( $dataType, $this->defaultValues ) ){
				throw new DataModelException( "BAD_DATATYPE" );
			}
			
			/* setting field definition */
			$this->def[ $fieldName ] = $dataType ;
			
			/* setting default value */
			$this->data[ $fieldName ] = $this->defaultValues[ $dataType ];
		}
		
		/* set - sets value for an existing field */
		function set( $fieldName, $value ){
			if( !array_key_exists( $fieldName, $this->def ) ){
				throw new DataModelException( "UNDEFINED_FIELD" );
			}
			
			$this->validate($fieldName,$this->def[$fieldName],$value);
			$this->data[ $fieldName ] = $value;
		}
		
		/* get - returns value of a defined field */
		function get( $fieldName ){
			if( !array_key_exists( $fieldName, $this->def ) ){
				throw new DataModelException( "UNDEFINED_FIELD" );
			}
			
			return $this->data[ $fieldName ];
		}
		
		/* getData returns all fields with their values */
		function getData(){
			return $this->data;
		}
		
		/* VALIDATION *********************************************************/
		function validate( $fieldName, $dataType, &$value ){
			switch( $dataType ){
				case self::TYPE_STRING:
					break;
				case self::TYPE_INT:
					$this->validateInt( $fieldName, $value );
					break;
				case self::TYPE_FLOAT:
					$this->validateFloat( $fieldName, $value );
					break;
				case self::TYPE_BOOLEAN:
					$this->validateBoolean( $fieldName, $value );
					break;
				case self::TYPE_DATE:
					$this->validateDate( $fieldName, $value );
					break;
				case self::TYPE_TIME:
					$this->validateTime( $fieldName, $value );
					break;
				default:
					throw new DataModelException( "BAD_DATATYPE" );
			}
		}
		
		function validateInt( $fieldName, $value ){
			if(!filter_var($value, FILTER_VALIDATE_INT)){
				throw DataValidationException('INT_REQUIRED',$fieldName);
			}
		}
		
		function validateFloat( $fieldName, $value ){
			if(!filter_var($value, FILTER_VALIDATE_FLOAT)){
				throw DataValidationException('FLOAT_REQUIRED',$fieldName);
			}
		}

		function validateBoolean( $fieldName, $value ){
			if(!filter_var($value, FILTER_VALIDATE_BOOLEAN)){
				throw DataValidationException('BOOLEAN_REQUIRED',$fieldName);
			}
		}

		function validateDate( $fieldName, $value ){
			$this->validateInt($fieldName,$value);
		}
		
		function validateTime( $fieldName, $value ){
			$max = 24*60*60;
			$this->validateInt($fieldName,$value);
			if( $value < 0 || $value > $max ){
				throw DataValidationException('BAD_TIME',$fieldName);
			}
		}
		
		/* DATABASE OPERATIONS ************************************************/
		
		function load( $id ){
			$sql = "select * from ".$this->tableName;
			$sql .=" where ".$this->idFieldName."=".$id;
			$result = $this->connectionObject->query( $sql );
			switch( $this->databaseType ){
				case self::DB_PGSQL:
					$this->loadFromPgsqlResult( $result );
					break;
				case self::DB_MYSQL:
					$this->loadFromMysqlResult( $result );
				default:
					throw new DataModelException("UNSUPPORTED_DATABASE");
			}
		}
		
		function loadFromPgsqlResult( &$result ){
			$found = false;
			while( $row = $this->connectionObject->fetchAssoc( $result )){
				$found = true;
				foreach( $row as $k=>$v ){
					$this->set($k,$v);
				}
			}
			
			if( !$found ){
				throw new DataModelException("RECORD_NOT_FOUND");
			}
		}
		
		function loadFromMysqlResult( &$result ){
			$found = false;
			while( $row = $result->fetch_assoc()){
				$found = true;
				foreach( $row as $k=>$v ){
					$this->set($k,$v);
				}
			}
			
			if( !$found ){
				throw new DataModelException("RECORD_NOT_FOUND");
			}
		}
		
		function insert(){
			$keys = array_keys( $this->def );
			unset( $keys[ $this->idFieldName ] );
			$values = array();
			foreach( $keys as $k ){
				if( $this->def[ $k ] == self::TYPE_STRING ){
					array_push( $values, "'".htmlentities($this->data[$k],ENT_QUOTES,'UTF-8')."'");
				}else{
					array_push( $values, $this->data[$k] );
				}
			}
			
			$sql = "insert into ".$this->tableName;
			$sql .=" (".implode(',',$keys).")";
			$sql .=" values";
			$sql .=" (".implode(',',$values).")";
			
			return $this->connectionObject->query( $sql );
		}
		
		function update(){
			if( $this->data[ $idFieldName ] == 0 ){
				throw new DataModelException("RECORD_NOT_LOADED");
			}
			
			$keys = array_keys( $this->def );
			unset( $keys[ $this->idFieldName ] );
			$values = array();
			foreach( $keys as $k ){
				if( $this->def[ $k ] == self::TYPE_STRING ){
					array_push( $values, $k."="."'".htmlentities($this->data[$k],ENT_QUOTES,'UTF-8')."'" );
				}else{
					array_push( $values, $k."=".$this->data[$k] );
				}
			}
			
			$sql = "update ".$this->tableName;
			$sql .=" set ".implode(',',$values);
			$sql .=" where ".$this->idFieldName."=".$this->data[ $this->idFieldName ];
			
			return $this->connectionObject->query( $sql );
		}
		
		function delete(){
			if( $this->data[ $idFieldName ] == 0 ){
				throw new DataModelException("RECORD_NOT_LOADED");
			}
			
			$sql = "delete from ".$this->tableName;
			$sql .=" where ".$this->idFieldName."=".$this->data[ $this->idFieldName ];
			
			return $this->connectionObject->query( $sql );
		}
		
		function getAllRecords(){
			$sql = "select * from ".$this->tableName." orderby ".$this->idFieldName;
			return $this->connectionObject( $sql );
		}
		
		function getPagedRecords( $orderBy, $order="asc", $pageNumber=1, $limit=30 ){
			$page= $page-1;
			if($page<0){
				$page=0;
			}
			
			if( !is_numeric($limit) || $limit<1){
				$limit=30;
			}
			
			$offset = $limit * $page;
			$sql = "select * from ".$this->tableName." orderby ".$orderBy." ".$order;
			$sql .=" limit $limit offset $offset";
			return $this->connectionObject( $sql );
		}
		
		function getRecordCount(){
			$count=0;
			$sql = "select count(*) as total_records from ".$this->tableName;
			$result = $this->connectionObject->query( $sql );
			if( $this->databaseType == self::DB_PGSQL ){
				while( $row = pg_fetch_assoc($result)){
					$count = $row['total_records'];
				}
			}else if( $this->databaseType == self::DB_MYSQL ){
				while( $row = $result->fetch_assoc() ){
					$count = $row['total_records'];
				}
			}
			
			return $count;
		}
	}
?>
