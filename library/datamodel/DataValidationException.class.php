<?php
	class DataValidationException extends Exception{
	
		protected $fieldName = '';
		
		function __construct( $message, $fieldName ){
			parent::__construct( $message );
			$this->fieldName = $fieldName;
		}
		
		function getField(){
			return $this->fieldName;
		}
	}
?>
