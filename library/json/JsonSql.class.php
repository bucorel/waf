<?php
	/* JsonSql class creates SQL statements for JsonBase data structures 
	DO NOT FORGET TO INCLUDE JsonBase CLASS FIRST */
	
	class JsonSql extends JsonBase{
		
		protected $tableName = "";
		protected $idFieldName = "";
		protected $docFieldName = "";
		
		function setTableName( $tableName ){
			$this->tableName = $tableName;
		}
		
		function setIdFieldName( $fieldName ){
			$this->idFieldName = $fieldName;
		}
		
		function setDocFieldName( $fieldName ){
			$this->docFieldName = $fieldName;
		}
		
		function getInsertSql(){
			$sql = "insert into ".$this->tableName;
			$sql .=" (".$this->docFieldName.") values ('";
			$sql .=json_encode($this->data)."')";
			$sql .=" returning ".$this->idFieldName;
			
			return $sql;
		}
		
		function getUpdateSql( $recordId ){
			$sql = "update ".$this->tableName;
			$sql .=" set ".$this->docFieldName."='";
			$sql .=json_encode($this->data)."'";
			$sql .=" where ".$this->idFieldName."=".$recordId;
			
			return $sql;
		}
		
		function getDeleteSql( $recordId ){
			$sql = "delete from ".$this->tableName;
			$sql .=" where ".$this->idFieldName."=".$recordId;
			
			return $sql;
		}
		
		function getListSql( $pageNumber=1, $limit=30, $orderBy="", 
								$orderType="asc", array $fieldsRequired ){
			$sql = "";
			$fieldSetBlock = "";
			$orderBlock = "";
			$paginationBlock = "";
			$recordOffset = 0;
			
			foreach( $fieldsRequired as $k => $v ){
				$fieldSetBlock .=$k." as ".$v;
			}
			
			if( $fieldSetBlock == "" ){
				$fieldSetBlock = "*";
			}
			
			if( $orderBy != "" ){
				$orderBlock = " order by ".$orderBy;
				if( $orderType == "asc" || $orderType == "desc" ){
					$orderBlock .=" ".$orderType;
				}
			}
			
			if( is_numeric( $pageNumber ) && is_numeric( $limit ) ){
				$pageToShow = $pageNumber - 1;
				$recordOffset = $limit * $pageToShow;
				if( $pageToShow >= 0 ){
					$paginationBlock = "limit ".$limit." offset ".$recordOffset;
				} 
			}
			
			$sql = "select $fieldSetBlock from ".$this->tableName;
			$sql .=$paginationBlock." ".$orderBlock;
			
			return $sql;
		}
	}
?>
