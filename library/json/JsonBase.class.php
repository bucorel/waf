<?php
	/*
	JsonBase class creates JSON based data structure. You can store this json
	in any json store like mongodb, couchdb, rethink or postgresql jsonb field
	*/
	
	class JsonBase{
		/* $def contains the data definition in a key->value pair where keys 
		represents field name and value represents data type */
		protected $def = array();
		
		/* $data holds actual data in key->value pair. Here keys represents 
		field name and value stores actual value. A key in $data array must
		exists in the $def array and must contain a value according to the data
		type defined in $def */
		protected $data = array();
		
		/* allowed data types */
		const TYPE_STRING = 0;
		const TYPE_INTEGER = 1;
		const TYPE_FLOAT = 2;
		const TYPE_BOOLEAN = 3;
		const TYPE_DATE = 4;
		const TYPE_TIME = 5;
		const TYPE_ARRAY = 6;
		
		/* defineField defines a field. A field must have a name, a datatype and
		a default value */
		function defineField( $fieldName, $dataType, $defaultValue = null ){
			$details = array();
			$details['dType'] = $dataType;
			$details['dValue'] = $defaultValue;
			if( !array_key_exists( $fieldName, $this->def ) ){
				$this->def[ $fieldName ] = $details;
				$this->data[ $fieldName ] = $defaultValue;
			}
		}
		
		/* set - sets a value for any existing field */
		function set( $fieldName, $value ){
			/* Lets check whether the field is defined or not */
			if( array_key_exists($fieldName, $this->def ) ){
				/* its time to validate the input to match defined data type */
				if( $this->validate( $this->def[ $fieldName ]['dType'], $value ) ){
					$this->data[ $fieldName ] = $value;
				}
			}
		}
		
		/* get - returns value of given field */
		function get( $fieldName ){
			if( array_key_exists( $fieldName, $this->data ) ){
				return $this->data[ $fieldName ];
			}
			
			return null;
		}
		
		/* load -loads an entire array. Set stores one field at a time while
		load stores all or more than one at a time */
		function load( array $a ){
			foreach( $a as $k=>$v ){
				$this->set( $k, $v );
			}
		}
		
		/* return true when datatype matches or false if it doesn't */
		function validate( $type, &$value ){
			switch( $type ){
				case self::TYPE_STRING:
					return true;
				case self::TYPE_INTEGER:
					return $this->isValidInt( $value );
				case self::TYPE_FLOAT:
					return $this->isValidFloat( $value );
				case self::TYPE_BOOLEAN:
					return $this->isValidBool( $value );
				case self::TYPE_DATE:
					return $this->isValidDate( $value );
				case self::TYPE_TIME:
					return $this->isValidTime( $value );
				case self::TYPE_ARRAY:
					return $this->isValidArray( $value );
				default:
					throw new Exception( 'INVALID_DATA_TYPE' );
			}
		}
		
		function isValidInt( &$value ){
			if((string)(int)$value == $value) {
				return true;
			}
			return false;
		}
		
		function isValidFloat( &$value ){
			if((string)(float)$value == $value) {
				return true;
			}
			return false;
		}

		/* here boolean means 1 or 0 */
		function isValidBool( &$value ){
			if( $value == 1 || $value == 0 ){
				return true;
			}
			return false;
		}
		
		/* for us date means unix timestamp (integer) */
		function isValidDate( &$value ){
			return $this->isValidInt( $value );
		}
		
		/* for us time is an integer from 0 to 86400 (24*60*60) */
		function isValidTime( &$value ){
			if( !$this->isValidInt( $value )){
				return false;
			}
			
			if( $value < 0 || $value > 86400 ){
				return false;
			}
			
			return true;
		}
		
		function isValidArray( &$value ){
			return is_array( $value );
		}
	}
?>
